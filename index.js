let trainer = {
	fullName: 'Ash Ketchum',
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		haenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	introduce: function(){
		console.log(trainer.pokemon[1] + ' I choose you!');
	}
}
console.log(trainer);
console.log('Result of dot notation:');
console.log(trainer.fullName);
console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);
console.log('Result of talk method:');
trainer.introduce();


let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log('This Pokemon tackled targetPokemon');
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	},
	faint: function(){
		console.log('Pokemon fainted');
	}
}

console.log(myPokemon);

function Pokemon(name, level){

	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log("Target Pokemon's health is now reduced to " + Number(target.health - this.attack));
	};
	this.faint = function(){
		console.log(this.name + ' fainted. ');
	};

}

// Create new instances of the 'Pokemon' object each with their unique properties
let pikachu = new Pokemon('Pikachu', 50);
console.log(pikachu);

let JigglyPuff = new Pokemon('JigglyPuff', 25);
console.log(JigglyPuff);

pikachu.tackle(JigglyPuff);
JigglyPuff.faint();

